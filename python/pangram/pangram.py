import string


def is_pangram(sentence):
    # create a lowercase set of all the letters in the alphabet
    alphabet = set(string.ascii_lowercase)

    # turn sentence lowercase, then compare a set of all characters
    # in the sentence with all the characters in the alphabet
    if set(sentence.lower()) >= alphabet:
        return True
    else:
        return False
